#!/usr/bin/env python2.7

import sys
import urllib2

def createURL(site):
    return('https://' + site + '.com/' + sys.argv[1])

sites = ['twitter', 'hackerone', 'gitlab', 'github']

print 'Checking availability for user name: ' + sys.argv[1]
for site in sites:
    urlundertest = createURL(site)

    try:
        response = urllib2.urlopen(urlundertest, timeout=7)
        if response.url != urlundertest and "suspended" not in response.url:
            print ('\033[1;32m' + u'\u2611 ' + site + '(' + urlundertest  + ')' + '\033[1;m')
        else:
            print('\033[1;31m' + u'\u2612 ' + site + '(' + urlundertest + ')' + '\033[1;m')

    except urllib2.HTTPError, e:
        print ('\033[1;32m' + u'\u2611 ' + site + '(' + urlundertest  + ')' + '\033[1;m')

    except urllib2.URLError, e:
        print ('\033[1;32m' + u'\u2611 ' + site + '(' + urlundertest  + ')' + '\033[1;m')
