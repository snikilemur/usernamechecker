FROM alpine:latest
FROM python:2

COPY usernamechecker.py /usernamechecker.py
ENTRYPOINT ["/usernamechecker.py"]

#CMD ["/usernamechecker.py"]
